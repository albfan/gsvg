/* gsvg-dom-interfaces-path.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;

namespace GSvg {

public interface PathSeg : Object {
  public abstract uint path_seg_type { get;}
  public abstract string path_seg_type_as_letter { get;}

  /**
   * Path Segment Types
   */
  public enum Type {
    UNKNOWN = 0,
    CLOSEPATH = 1,
    MOVETO_ABS = 2,
    MOVETO_REL = 3,
    LINETO_ABS = 4,
    LINETO_REL = 5,
    CURVETO_CUBIC_ABS = 6,
    CURVETO_CUBIC_REL = 7,
    CURVETO_QUADRATIC_ABS = 8,
    CURVETO_QUADRATIC_REL = 9,
    ARC_ABS = 10,
    ARC_REL = 11,
    LINETO_HORIZONTAL_ABS = 12,
    LINETO_HORIZONTAL_REL = 13,
    LINETO_VERTICAL_ABS = 14,
    LINETO_VERTICAL_REL = 15,
    CURVETO_CUBIC_SMOOTH_ABS = 16,
    CURVETO_CUBIC_SMOOTH_REL = 17,
    CURVETO_QUADRATIC_SMOOTH_ABS = 18,
    CURVETO_QUADRATIC_SMOOTH_REL = 19
  }
}

public interface PathSegClosePath : Object, PathSeg {
}

public interface PathSegMovetoAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface PathSegMovetoRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface PathSegLinetoAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface PathSegLinetoRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface PathSegCurvetoCubicAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface PathSegCurvetoCubicRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface PathSegCurvetoQuadraticAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
}

public interface PathSegCurvetoQuadraticRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
}

public interface PathSegArcAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double r1 { get; set; }
  public abstract double r2 { get; set; }
  public abstract double angle { get; set; }
  public abstract bool large_arc_flag { get; set; }
  public abstract bool sweep_flag { get; set; }
}

public interface PathSegArcRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double r1 { get; set; }
  public abstract double r2 { get; set; }
  public abstract double angle { get; set; }
  public abstract bool large_arc_flag { get; set; }
  public abstract bool sweep_flag { get; set; }
}

public interface PathSegLinetoHorizontalAbs : Object, PathSeg {
  public abstract double x { get; set; }
}

public interface PathSegLinetoHorizontalRel : Object, PathSeg {
  public abstract double x { get; set; }
}

public interface PathSegLinetoVerticalAbs : Object, PathSeg {
  public abstract double y { get; set; }
}

public interface PathSegLinetoVerticalRel : Object, PathSeg {
  public abstract double y { get; set; }
}

public interface PathSegCurvetoCubicSmoothAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface PathSegCurvetoCubicSmoothRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface PathSegCurvetoQuadraticSmoothAbs : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface PathSegCurvetoQuadraticSmoothRel : Object, PathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface PathSegList : Object {

  public abstract uint numberOfItems { get;}

  public abstract void clear() throws GLib.Error;
  public abstract PathSeg initialize (PathSeg newItem) throws GLib.Error;
  public abstract PathSeg get_item (uint index) throws GLib.Error;
  public abstract PathSeg insert_item_before (PathSeg newItem, uint index) throws GLib.Error;
  public abstract PathSeg replace_item (PathSeg newItem, uint index) throws GLib.Error;
  public abstract PathSeg remove_item (uint index) throws GLib.Error;
  public abstract PathSeg append_item (PathSeg newItem) throws GLib.Error;
}

public interface AnimatedPathData : Object {
  public abstract  PathSegList path_seg_list { get;}
  public abstract  PathSegList normalized_path_seg_list { get;}
  public abstract  PathSegList animated_path_seg_list { get;}
  public abstract  PathSegList animated_normalized_path_seg_list { get;}
}

public interface PathElement : Object, Element,
                           Tests,
                           LangSpace,
                           ExternalResourcesRequired,
                           Stylable,
                           Transformable,
                           AnimatedPathData {

  public abstract AnimatedNumber path_length { get; construct set; }

  public abstract double get_total_length ();
  public abstract Point get_point_at_length (double distance);
  public abstract uint get_path_seg_at_length (double distance);
  public abstract PathSegClosePath create_svg_path_seg_close_path ();
  public abstract PathSegMovetoAbs create_svg_path_seg_moveto_abs (double x, double y);
  public abstract PathSegMovetoRel create_svg_path_seg_moveto_rel (double x, double y);
  public abstract PathSegLinetoAbs create_svg_path_seg_lineto_abs (double x, double y);
  public abstract PathSegLinetoRel create_svg_path_seg_lineto_rel (double x, double y);
  public abstract PathSegCurvetoCubicAbs create_svg_path_seg_curveto_cubic_abs (double x, double y, double x1, double y1, double x2, double y2);
  public abstract PathSegCurvetoCubicRel create_svg_path_seg_curveto_cubic_rel (double x, double y, double x1, double y1, double x2, double y2);
  public abstract PathSegCurvetoQuadraticAbs create_svg_path_seg_curveto_quadratic_abs (double x, double y, double x1, double y1);
  public abstract PathSegCurvetoQuadraticRel create_svg_path_seg_curveto_quadratic_rel (double x, double y, double x1, double y1);
  public abstract PathSegArcAbs create_svg_path_seg_arc_abs (double x, double y, double r1, double r2, double angle, bool largeArcFlag, bool sweepFlag);
  public abstract PathSegArcRel create_svg_path_seg_arc_rel (double x, double y, double r1, double r2, double angle, bool largeArcFlag, bool sweepFlag);
  public abstract PathSegLinetoHorizontalAbs create_svg_path_seg_lineto_horizontal_abs (double x);
  public abstract PathSegLinetoHorizontalRel create_svg_path_seg_lineto_horizontal_rel (double x);
  public abstract PathSegLinetoVerticalAbs create_svg_path_seg_lineto_vertical_abs (double y);
  public abstract PathSegLinetoVerticalRel create_svg_path_seg_lineto_vertical_rel (double y);
  public abstract PathSegCurvetoCubicSmoothAbs create_svg_path_seg_curveto_cubic_smooth_abs (double x, double y, double x2, double y2);
  public abstract PathSegCurvetoCubicSmoothRel create_svg_path_seg_curveto_cubic_smooth_rel (double x, double y, double x2, double y2);
  public abstract PathSegCurvetoQuadraticSmoothAbs create_svg_path_seg_curveto_quadratic_smooth_abs (double x, double y);
  public abstract PathSegCurvetoQuadraticSmoothRel create_svg_path_seg_curveto_quadratic_smooth_rel (double x, double y);
}


public interface PathElementMap : Object {
  public abstract int length { get; }
  public abstract PathElement get (string id);
  public abstract void append (PathElement el);
}

} // GSvg
