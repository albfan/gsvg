/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-image.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public class GSvg.GsImageElement : GsCommonShapeElement,
                            URIReference,
                            Transformable, ImageElement {
  // Transformable
  public AnimatedTransformList transform { get; set; }
  [Description (nick="::transform")]
  public GsAnimatedTransformList mtransform {
    get { return transform as GsAnimatedTransformList; }
    set { transform = value as AnimatedTransformList; }
  }
  // URIReference
  public AnimatedString href { get; set; }
  [Description (nick="::href")]
  public GsAnimatedString mhref {
    get { return href as GsAnimatedString; }
    set { href = value as AnimatedString; }
  }
  // ImageElement
  public AnimatedLength x { get; set; }
  [Description (nick="::x")]
  public GsAnimatedLength mx {
    get { return x as GsAnimatedLength; }
    set { x = value as AnimatedLength; }
  }
  public AnimatedLength y { get; set; }
  [Description (nick="::y")]
  public GsAnimatedLength my {
    get { return y as GsAnimatedLength; }
    set { y = value as AnimatedLength; }
  }
  public AnimatedLength width { get; set; }
  [Description (nick="::width")]
  public GsAnimatedLength mwidth {
    get { return width as GsAnimatedLength; }
    set { width = value as AnimatedLength; }
  }
  public AnimatedLength height { get; set; }
  [Description (nick="::height")]
  public GsAnimatedLength mheight {
    get { return height as GsAnimatedLength; }
    set { height = value as AnimatedLength; }
  }
  public AnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
  [Description (nick="::preserveAspectRatio")]
  public GsAnimatedPreserveAspectRatio mpreserve_aspect_ratio {
    get { return preserve_aspect_ratio as GsAnimatedPreserveAspectRatio; }
    set { preserve_aspect_ratio = value as AnimatedPreserveAspectRatio; }
  }
}
