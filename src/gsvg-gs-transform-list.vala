/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;
using GXml;

public class GSvg.GsTransformList : ArrayList<Transform>,
                                  GomProperty,
                                  TransformList
{

  static string KEYWORDS = "matrix translate scale rotate skewX skewY";
  private string separator = " ";
  public int number_of_items { get { return size; } }

  public new void  clear () throws GLib.Error { (this as ArrayList).clear (); }
  public Transform initialize (Transform new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }
  public Transform get_item (int index) throws GLib.Error {
    return get (index);
  }
  public Transform insert_item_before (Transform new_item, int index) throws GLib.Error {
    insert (index, new_item);
    return new_item;
  }
  public Transform replace_item (Transform new_item, int index) throws GLib.Error {
    remove_at (index);
    insert (index, new_item);
    return new_item;
  }
  public Transform remove_item (int index) throws GLib.Error {
    return remove_at (index);
  }
  public Transform append_item (Transform new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }
  public string? value {
    set {
      if (value == null) {
        try { clear (); } catch (GLib.Error e) { warning ("Error: "+e.message); }
        return;
      }
      parse (value);
    }
    owned get {
      if (size == 0) return null;
      string str = "";
      for (int i = 0; i < size; i++) {
        var p = get (i);
        str += p.to_string ();
        if (i+1 < size) str += separator;
      }
      return str;
    }
  }
  public bool validate_value (string val) {
    return "," in val || " " in val; // FIXME
  }
  public Transform create_svg_transform_from_matrix (Matrix matrix) { return new GsTransform (); }
  public Transform consolidate () throws GLib.Error { return new GsTransform (); }
  private void parse (string str) {
    try {
      clear ();
      string kw = "";
      bool skip = false;
      int i = 0;
      unichar c;
      while (str.get_next_char (ref i, out c)) {
        kw = "";
        skip = false;
        while (c.to_string () != "(") {
          kw += c.to_string ();
          if (!str.get_next_char (ref i, out c)) return;
        }
        kw = kw.strip ();
        if (!(kw.replace("(","") in KEYWORDS)) skip = true;
        kw += c.to_string();
        if (!str.get_next_char (ref i, out c)) return;
        while (c.to_string () != ")") {
          kw += c.to_string ();
          if (!str.get_next_char (ref i, out c)) return;
        }
        kw += c.to_string();
        if (!skip) {
          var tr = new GsTransform ();
          tr.parse (kw);
          add (tr);
        }
        while (c.to_string () == " ") {
          if (!str.get_next_char (ref i, out c)) return;
        }
      }
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
}

