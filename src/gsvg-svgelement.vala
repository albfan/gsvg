/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016-2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GXml;
using Gee;

/**
 * Top level SVG Element node according with [[https://www.w3.org/TR/SVG/]] version 1.1
 */
public interface GSvg.SVGElement : Object,
                          Element,
                          Tests,
                          LangSpace,
                          ExternalResourcesRequired,
                          Stylable,
                          Locatable,
                          FitToViewBox,
                          ZoomAndPan,
                          ViewCSS,
                          DocumentCSS,
                          ContainerElement {

  public abstract AnimatedLength x { get; set; }
  public abstract AnimatedLength y { get;  set; }
  public abstract AnimatedLength width { get;  set; }
  public abstract AnimatedLength height { get;  set; }
  public abstract string content_script_type { get; set; }
  public abstract string content_style_type { get; set; }
  public abstract Rect viewport { get; set; }
  public abstract float pixel_unit_to_millimeter_x { get; }
  public abstract float pixel_unit_to_millimeter_y { get; }
  public abstract float screen_pixel_to_millimeter_x { get; }
  public abstract float screen_pixel_to_millimeter_y { get; }
  public abstract bool use_current_view { get; }
  public abstract ViewSpec current_view { get; }
  public abstract float current_scale { get; set; }
  public abstract Point current_translate { get; }

  public abstract uint suspend_redraw (uint maxWaitMilliseconds);
  public abstract void unsuspend_redraw (uint suspendHandleID);
  public abstract void unsuspend_redrawAll ();
  public abstract void force_redraw ();
  public abstract void pause_animations ();
  public abstract void unpause_animations ();
  public abstract bool animations_paused ();
  public abstract float get_current_time ();
  public abstract void set_current_time (float seconds);
  public abstract DomNodeList get_intersection_list (Rect rect, Element referenceElement);
  public abstract DomNodeList get_enclosure_list (Rect rect, Element referenceElement);
  public abstract bool check_intersection (Element element, Rect rect);
  public abstract bool check_enclosure (Element element, Rect rect);
  public abstract void deselect_all ();
  public abstract Number create_svg_number ();
  public abstract Length create_svg_length ();
  public abstract Angle create_svg_angle ();
  public abstract Point create_svg_point ();
  public abstract Matrix create_svg_matrix ();
  public abstract Rect create_svg_rect ();
  public abstract Transform create_svg_transform ();
  public abstract Transform create_svg_transform_from_matrix (Matrix matrix);
  public abstract DomElement? get_element_by_id (string elementId);
  // API additions
  public abstract void read_from_file (GLib.File file) throws GLib.Error;
  public abstract void read_from_string (string str) throws GLib.Error;
  public abstract string write_string () throws GLib.Error;
  // Shapes creation
  /**
   * Creates a 'rect' node for rectangular shapes.
   *
   * @param x a string representation of an {@link AnimatedLengthX}
   * @param y a string representation of an {@link AnimatedLengthY}
   * @param width a string representation of an {@link AnimatedLengthWidth}
   * @param height a string representation of an {@link AnimatedLengthHeight}
   * @param rx a string representation of an {@link AnimatedLengthRX}
   * @param ry a string representation of an {@link AnimatedLengthRY}
   */
  public abstract RectElement create_rect (string? x,
                                  string? y,
                                  string? width,
                                  string? height,
                                  string? rx,
                                  string? ry,
                                  string? style = null);
  /**
   * Creates a 'circle' node for circle shapes.
   *
   * @param cx a string representation of an {@link AnimatedLengthCX}
   * @param cy a string representation of an {@link AnimatedLengthCY}
   * @param cr a string representation of an {@link AnimatedLengthR}
   * @param style a string with style information
   */
  public abstract CircleElement create_circle (string? cx,
                                  string? cy,
                                  string? cr,
                                  string? style = null);
  /**
   * Creates a 'ellipse' node for ellipse shapes.
   *
   * @param cx a string representation of an {@link AnimatedLengthCX}
   * @param cy a string representation of an {@link AnimatedLengthCY}
   * @param crx a string representation of an {@link AnimatedLengthRX}
   * @param cry a string representation of an {@link AnimatedLengthRY}
   * @param style a string with style information
   */
  public abstract EllipseElement create_ellipse (string? cx,
                                  string? cy,
                                  string? crx,
                                  string? cry,
                                  string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param lx1 a string representation of an {@link AnimatedLengthCX}
   * @param lx2 a string representation of an {@link AnimatedLengthCY}
   * @param lx1 a string representation of an {@link AnimatedLengthRX}
   * @param ly2 a string representation of an {@link AnimatedLengthRY}
   * @param style a string with style information
   */
  public abstract LineElement create_line (string? lx1,
                                  string? ly1,
                                  string? lx2,
                                  string? ly2,
                                  string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param points a string representation of a list of {@link Point}
   * @param style a string with style information
   */
  public abstract PolylineElement create_polyline (string points,
                                   string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param points a string representation of a list of {@link Point}
   * @param style a string with style information
   */
  public abstract PolygonElement create_polygon (string points,
                                   string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param text a text to be displayed
   * @param xs a list of coordinates
   * @param ys a list of coordinates
   * @param dxs a list of coordinates
   * @param dys a list of coordinates
   * @param rotates a list of numbers
   * @param style a string with style information
   */
  public abstract TextElement create_text (string? text,
                                   string? xs,
                                   string? ys,
                                   string? dxs,
                                   string? dys,
                                   string? rotates,
                                   string? style = null);
  /**
   * Creates a 'g' node.
   */
  public abstract GElement create_g ();
  /**
   * Adds a definitions element node.
   */
  public abstract DefsElement add_defs ();
  /**
   * Adds a grouping element node.
   */
  public abstract GElement add_g ();
  /**
   * Adds a 'title' node to current SVG one.
   *
   * @param text a title to be added
   */
  public abstract TitleElement add_title (string text);
  /**
   * Adds a description 'desc' node to current SVG one.
   *
   * @param text a text to be added to description or null, if custome values will be added
   */
  public abstract DescElement add_desc (string? text);
  /**
   * Adds a 'metadata' node to current SVG one.
   */
  public abstract MetadataElement add_metadata ();

  /**
   * Adds an 'svg' element to the this.
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link AnimatedLength}, for x position, or null
   * @param y a string representation of {@link AnimatedLength}, for y position, or null
   * @param width a string representation of {@link AnimatedLength}, for width, or null
   * @param height a string representation of {@link AnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link AnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract SVGElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
  /**
   * Creates a detached 'svg' element. It then can be appended as a child
   * of container elements.
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link AnimatedLength}, for x position, or null
   * @param y a string representation of {@link AnimatedLength}, for y position, or null
   * @param width a string representation of {@link AnimatedLength}, for width, or null
   * @param height a string representation of {@link AnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link AnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract SVGElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
  /**
   * Creates a detached 'path' element. It then can be appended as a child
   * of container elements.
   *
   * @param d a string representation path's defintion
   * @param length a string representation of {@link AnimatedNumber}, for path length, or null
   */
  public abstract PathElement create_path (string d, string? length);
  /**
   * Moves an element to given position, only if it has an x and y like properties.
   */
  public abstract void move (GSvg.Element e, PointLength p);
  /**
   * Moves a delta distance of element, only if it has an x and y like properties.
   * Current x and y units are used as type.
   */
  public abstract void move_delta (GSvg.Element e, double x, double y);
  /**
   * Iterates over SVGs, lines and rectangles, to find minimum width, in milimeters, required to show
   * all of them. No transformation is considered.
   */
  public virtual double calculate_width () {
    double w = 0.0;
    foreach (string key in (svgs as GomHashMap).get_keys ()) {
      var s = svgs.get (key) as SVGElement;
      if (s == null) {
        continue;
      }
      double sw = 0.0;
      if (s.width != null) {
        sw = s.width.base_val.value;
        if (s.x != null) {
          sw += s.x.base_val.value;
        }
        if (sw > w) {
          w = sw;
        }
      } else {
        sw = s.calculate_width ();
        if (sw > w) {
          w = sw;
        }
      }
    }
    foreach (string key in (lines as GomHashMap).get_keys ()) {
      var l = lines.get (key) as LineElement;
      if (l == null) {
        continue;
      }
      double lw = 0.0;
      if (l.x1 != null && l.x2 != null) {
        if (l.x2.base_val.value > l.x1.base_val.value) {
          lw = l.x2.base_val.value;
        } else {
          lw = l.x1.base_val.value;
        }
      }
      if (lw > w) {
        w = lw;
      }
    }
    foreach (string key in (rects as GomHashMap).get_keys ()) {
      var r = rects.get (key) as RectElement;
      if (r == null) {
        continue;
      }
      double rw = 0.0;
      if (r.width != null) {
        rw = r.width.base_val.value;
      }
      if (r.x != null) {
        rw += r.x.base_val.value;
      }
      if (rw > w) {
        w = rw;
      }
    }
    foreach (string key in (circles as GomHashMap).get_keys ()) {
      var c = circles.get (key) as CircleElement;
      if (c == null) {
        continue;
      }
      double rw = 0.0;
      if (c.r != null) {
        rw = c.r.base_val.value;
      }
      if (c.cx != null) {
        rw += c.cx.base_val.value;
      }
      if (rw > w) {
        w = rw;
      }
    }
    foreach (string key in (ellipses as GomHashMap).get_keys ()) {
      var e = ellipses.get (key) as EllipseElement;
      if (e == null) {
        continue;
      }
      double rw = 0.0;
      if (e.rx != null) {
        rw = e.rx.base_val.value;
      }
      if (e.cx != null) {
        rw += e.cx.base_val.value;
      }
      if (rw > w) {
        w = rw;
      }
    }
    return w;
  }
  /**
   * Iterates over SVGs, lines and rectangles, to find minimum height, in milimeters, required to show
   * all of them. No transformation is considered.
   */
  public virtual double calculate_height () {
    double h = 0.0;
    foreach (string key in (svgs as GomHashMap).get_keys ()) {
      var s = svgs.get (key) as SVGElement;
      if (s == null) {
        continue;
      }
      double sh = 0.0;
      if (s.height != null) {
        sh = s.height.base_val.value;
        if (s.y != null) {
          sh += s.y.base_val.value;
        }
        if (sh > h) {
          h = sh;
        }
      } else {
        sh = s.calculate_height ();
        if (sh > h) {
          h = sh;
        }
      }
    }
    foreach (string key in (lines as GomHashMap).get_keys ()) {
      var l = lines.get (key) as LineElement;
      if (l == null) {
        continue;
      }
      double lh = 0.0;
      if (l.y1 != null && l.y2 != null) {
        if (l.y2.base_val.value > l.y1.base_val.value) {
          lh = l.y2.base_val.value;
        } else {
          lh = l.y1.base_val.value;
        }
      }
      if (lh > h) {
        h = lh;
      }
    }
    foreach (string key in (rects as GomHashMap).get_keys ()) {
      var r = rects.get (key) as RectElement;
      if (r == null) {
        continue;
      }
      double rh = 0.0;
      if (r.height != null) {
        rh = r.height.base_val.value;
      }
      if (r.y != null) {
        rh += r.y.base_val.value;
      }
      if (rh > h) {
        h = rh;
      }
    }
    foreach (string key in (circles as GomHashMap).get_keys ()) {
      var c = circles.get (key) as CircleElement;
      if (c == null) {
        continue;
      }
      double rh = 0.0;
      if (c.r != null) {
        rh = c.r.base_val.value;
      }
      if (c.cy != null) {
        rh += c.cy.base_val.value;
      }
      if (rh > h) {
        h = rh;
      }
    }
    foreach (string key in (ellipses as GomHashMap).get_keys ()) {
      var e = ellipses.get (key) as EllipseElement;
      if (e == null) {
        continue;
      }
      double rh = 0.0;
      if (e.ry != null) {
        rh = e.ry.base_val.value;
      }
      if (e.cy != null) {
        rh += e.cy.base_val.value;
      }
      if (rh > h) {
        h = rh;
      }
    }
    return h;
  }
}

/**
 * Hash table collection of {@link SVGElement} elements using its
 * id as key.
 */
public interface GSvg.SVGElementMap : Object {
  public abstract int length { get; }
  public abstract SVGElement get (string id);
  public abstract void append (SVGElement el);
}
