/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

/**
 * Base class for SVG and basic types elements
 */
public class GSvg.GsCommonElement : GsElement,
                        Tests,
                        LangSpace,
                        ExternalResourcesRequired
{
  // Tests
  protected StringList _required_features;
  protected StringList _required_extensions;
  protected StringList _system_language;
  protected AnimatedBoolean _external_resources_required;
  protected Element _nearest_viewport_element;
  protected Element _farthest_viewport_element;
    // requiredFeatures
  public StringList required_features { get { return _required_features;} }
  // requiredExtensions
  public StringList required_extensions { get { return _required_extensions; } }
  // systemLanguage
  public StringList system_language { get { return _system_language; } }

  public bool has_extension (string extension) { return false; }
  // LangSpace
  [Description (nick="::xml:lang")]
  public string xmllang { get; set; }
  [Description (nick="::xml:space")]
  public string xmlspace { get; set; }
  // ExternalResourcesRequired
  // externalResourcesRequired
  public AnimatedBoolean external_resources_required {
    get { return _external_resources_required; }
  }
}