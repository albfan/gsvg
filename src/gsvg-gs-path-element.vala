/* gsvg-gs-path-element.vala
 *
 * Copyright (C) 2016-2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GXml;

public class GSvg.GsPathElement : GsTransformable, AnimatedPathData, PathElement
{
  private PathSegList _path_seg_list;
  private PathSegList _normalized_path_seg_list;
  private PathSegList _animated_path_seg_list;
  private PathSegList _animated_normalized_path_seg_list;

  [Description (nick="::pathLength")]
  public GsAnimatedNumber mpath_length { get; set; }


  [Description (nick="::d")]
  public string d { get; set; }


  public AnimatedNumber path_length {
    get { return mpath_length as AnimatedNumber; }
    construct set {
      if (value is GomProperty) {
        mpath_length.value = (@value as GomProperty).value;
      }
    }
  }
  construct {
    initialize ("path");
  }

  public double get_total_length () {
    return 0.0;
  }
  public Point get_point_at_length (double distance) {
    return new GsPoint ();
  }
  public uint get_path_seg_at_length (double distance) {
    return 0;
  }
  public PathSegClosePath create_svg_path_seg_close_path () {
    return new GsPathSegClosePath ();
  }
  public PathSegMovetoAbs create_svg_path_seg_moveto_abs (double x, double y) {
    return new GsPathSegMovetoAbs ();
  }
  public PathSegMovetoRel create_svg_path_seg_moveto_rel (double x, double y) {
    return new GsPathSegMovetoRel ();
  }
  public PathSegLinetoAbs create_svg_path_seg_lineto_abs (double x, double y) {
    return new GsPathSegLinetoAbs ();
  }
  public PathSegLinetoRel create_svg_path_seg_lineto_rel (double x, double y) {
    return new GsPathSegLinetoRel ();
  }
  public PathSegCurvetoCubicAbs
    create_svg_path_seg_curveto_cubic_abs (double x, double y, double x1, double y1, double x2, double y2)
  {
    return new GsPathSegCurvetoCubicAbs ();
  }
  public PathSegCurvetoCubicRel
    create_svg_path_seg_curveto_cubic_rel (double x, double y, double x1, double y1, double x2, double y2)
  {
    return new GsPathSegCurvetoCubicRel ();
  }
  public PathSegCurvetoQuadraticAbs
    create_svg_path_seg_curveto_quadratic_abs (double x, double y, double x1, double y1)
  {
    return new GsPathSegCurvetoQuadraticAbs ();
  }
  public PathSegCurvetoQuadraticRel
    create_svg_path_seg_curveto_quadratic_rel (double x, double y, double x1, double y1)
  {
    return new GsPathSegCurvetoQuadraticRel ();
  }
  public PathSegArcAbs create_svg_path_seg_arc_abs (double x,
                                                    double y,
                                                    double r1,
                                                    double r2,
                                                    double angle,
                                                    bool largeArcFlag,
                                                    bool sweepFlag)
  {
    return new GsPathSegArcAbs ();
  }
  public PathSegArcRel create_svg_path_seg_arc_rel (double x,
                                                    double y,
                                                    double r1,
                                                    double r2,
                                                    double angle,
                                                    bool largeArcFlag,
                                                    bool sweepFlag)
  {
    return new GsPathSegArcRel ();
  }
  public PathSegLinetoHorizontalAbs create_svg_path_seg_lineto_horizontal_abs (double x) {
    return new GsPathSegLinetoHorizontalAbs ();
  }
  public PathSegLinetoHorizontalRel create_svg_path_seg_lineto_horizontal_rel (double x) {
    return new GsPathSegLinetoHorizontalRel ();
  }
  public PathSegLinetoVerticalAbs create_svg_path_seg_lineto_vertical_abs (double y) {
    return new GsPathSegLinetoVerticalAbs ();
  }
  public PathSegLinetoVerticalRel create_svg_path_seg_lineto_vertical_rel (double y) {
    return new GsPathSegLinetoVerticalRel ();
  }
  public PathSegCurvetoCubicSmoothAbs
    create_svg_path_seg_curveto_cubic_smooth_abs (double x,
                                                  double y,
                                                  double x2,
                                                  double y2)
  {
    return new GsPathSegCurvetoCubicSmoothAbs ();
  }
  public PathSegCurvetoCubicSmoothRel
    create_svg_path_seg_curveto_cubic_smooth_rel (double x,
                                                  double y,
                                                  double x2,
                                                  double y2)
  {
    return new GsPathSegCurvetoCubicSmoothRel ();
  }
  public PathSegCurvetoQuadraticSmoothAbs
    create_svg_path_seg_curveto_quadratic_smooth_abs (double x, double y)
  {
    return new GsPathSegCurvetoQuadraticSmoothAbs ();
  }
  public PathSegCurvetoQuadraticSmoothRel
    create_svg_path_seg_curveto_quadratic_smooth_rel (double x, double y)
  {
    return new GsPathSegCurvetoQuadraticSmoothRel ();
  }

  // AnimatedPathData
  public PathSegList path_seg_list { get { return _path_seg_list; } }
  public PathSegList normalized_path_seg_list { get { return _normalized_path_seg_list; } }
  public PathSegList animated_path_seg_list { get { return _animated_path_seg_list; } }
  public PathSegList animated_normalized_path_seg_list { get { return _animated_normalized_path_seg_list; } }
}


public class GSvg.GsPathElementMap : GomHashMap, PathElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct {
    try {
      initialize (typeof (GsPathElement));
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public new PathElement PathElementMap.get (string id) {
    return (this as GomHashMap).get (id) as PathElement;
  }
  public new void append (PathElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to paths collection: %s", e.message);
    }
  }
}

