/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;

public class GSvg.GsRectElement : GSvg.GsTransformable,
                          GSvg.RectElement, MappeableElement
{
  // RectElement
  public AnimatedLengthX x { get; set; }
  [Description (nick="::x")]
  public GsAnimatedLengthX mx {
    get { return x as GsAnimatedLengthX; }
    set { x = value as AnimatedLengthX; }
  }
  public AnimatedLengthY y { get; set; }
  [Description (nick="::y")]
  public GsAnimatedLengthY my {
    get { return y as GsAnimatedLengthY; }
    set { y = value as AnimatedLengthY; }
  }
  public AnimatedLengthWidth width { get; set; }
  [Description (nick="::width")]
  public GsAnimatedLengthWidth mwidth {
    get { return width as GsAnimatedLengthWidth; }
    set { width = value as AnimatedLengthWidth; }
  }
  public AnimatedLengthHeight height{ get; set; }
  [Description (nick="::height")]
  public GsAnimatedLengthHeight mheight {
    get { return height as GsAnimatedLengthHeight; }
    set { height = value as AnimatedLengthHeight; }
  }
  public AnimatedLengthRX rx { get; set; }
  [Description (nick="::rx")]
  public GsAnimatedLengthRX mrx {
    get { return rx as GsAnimatedLengthRX; }
    set { rx = value as AnimatedLengthRX; }
  }
  public AnimatedLengthRY ry { get; set; }
  [Description (nick="::ry")]
  public GsAnimatedLengthRY mry {
    get { return ry as GsAnimatedLengthRY; }
    set { ry = value as AnimatedLengthRY; }
  }
  construct {
    initialize ("rect");
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

