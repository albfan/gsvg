/* gsvg-dom-public public interfaces-filter.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * aint with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;

namespace GSvg {

public interface CursorElement : Object, Element,
                             URIReference,
                             Tests,
                             ExternalResourcesRequired {
  public abstract AnimatedLength x { get; }
  public abstract AnimatedLength y { get; }
}

public interface AElement : Object, Element,
                        URIReference,
                        Tests,
                        LangSpace,
                        ExternalResourcesRequired,
                        Stylable,
                        Transformable {
  public abstract AnimatedString target { get; }
}

public interface ViewElement : Object, Element,
                           ExternalResourcesRequired,
                           FitToViewBox,
                           ZoomAndPan {
  public abstract StringList view_target { get; }
}

public interface ScriptElement : Object,
                             Element,
                             URIReference,
                             ExternalResourcesRequired {
  public abstract string stype { get; set; }
}

public interface ZoomEvent : Object/*, UIEvent*/ {
  public abstract Rect zoom_rect_screen { get; }
  public abstract double previous_scale { get; }
  public abstract Point previous_translate { get; }
  public abstract double new_scale { get; }
  public abstract Point new_translate { get; }
}

public interface ElementTimeControl : Object {
  public abstract void begin_element();
  public abstract void begin_element_at(double offset);
  public abstract void end_element();
  public abstract void end_element_at(double offset);
}

public interface TimeEvent : Object, DomEvent {

// TODO: This is not devined in DOM4
//  public abstract AbstractView view { get; }
  public abstract int detail { get; }
// TODO: This is not devined in DOM4
//  void initTimeEvent(string typeArg, AbstractView viewArg, int detailArg);
}

public interface AnimationElement : Object, Element,
                                Tests,
                                ExternalResourcesRequired,
                                ElementTimeControl {

  public abstract Element targetElement { get; }

  public abstract double get_start_time ()throws GLib.Error;
  public abstract double get_current_time ();
  public abstract double get_simple_duration ()throws GLib.Error;
}

public interface AnimateElement : Object, AnimationElement,
                              Stylable {
}

public interface SetElement : Object, AnimationElement {
}

public interface AnimateMotionElement : Object, AnimationElement {
}

public interface MPathElement : Object, Element,
                            URIReference,
                            ExternalResourcesRequired {
}

public interface AnimateColorElement : Object, AnimationElement,
                                   Stylable {
}

public interface AnimateTransformElement : Object, AnimationElement {
}

} // GSvg
