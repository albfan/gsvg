/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces-struct.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gee;

namespace GSvg {
public interface Document : Object,  DomDocument {
  public abstract string title { owned get; }
  public abstract string referrer { get; }
  public abstract string domain { get; }
  public abstract string url { get; }
  public abstract SVGElement? root_element { owned get; }

  public abstract void read_from_string (string str) throws GLib.Error;
  public abstract void read_from_file (GLib.File file) throws GLib.Error;
  public abstract string write_string () throws GLib.Error;

  /**
   * Adds an 'svg' to the document. If no top most element in the tree exists,
   * this will be the one; it one exists already, a new {@link SVGElement} child
   * will appended to {@link root_element}
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link AnimatedLength}, for x position, or null
   * @param y a string representation of {@link AnimatedLength}, for y position, or null
   * @param width a string representation of {@link AnimatedLength}, for width, or null
   * @param height a string representation of {@link AnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link AnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract SVGElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
  /**
   * Creates a detached 'svg' element. It then can be appended as a child
   * of container elements.
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link AnimatedLength}, for x position, or null
   * @param y a string representation of {@link AnimatedLength}, for y position, or null
   * @param width a string representation of {@link AnimatedLength}, for width, or null
   * @param height a string representation of {@link AnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link AnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract SVGElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
}

public interface GElement : Object,
                        Element,
                        Tests,
                        LangSpace,
                        ExternalResourcesRequired,
                        Stylable,
                        Transformable,
                        ContainerElement {
}

public interface GElementMap : Object {
  public abstract int length { get; }
  public abstract GElement get (string id);
  public abstract void append (GElement el);
}

public interface DefsElement : Object,
                             Element,
                             Tests,
                             LangSpace,
                             ExternalResourcesRequired,
                             Stylable,
                             Transformable {
}

public interface DescElement : Object,
                           Element,
                           LangSpace,
                           Stylable {
}

public interface TitleElement : Object,
                            Element,
                            LangSpace,
                            Stylable {
}

public interface SymbolElement : Object,
                             Element,
                             LangSpace,
                             ExternalResourcesRequired,
                             Stylable,
                             FitToViewBox {
}

public interface UseElement : Element,
                          URIReference,
                          Tests,
                          LangSpace,
                          ExternalResourcesRequired,
                          Stylable,
                          Transformable {
  public abstract AnimatedLength x { get; }
  public abstract AnimatedLength y { get; }
  public abstract AnimatedLength width { get; }
  public abstract AnimatedLength height { get; }
  public abstract ElementInstance instance_root { get; }
  public abstract ElementInstance animated_instance_root { get; }
}

public interface ElementInstance : Object, DomEventTarget {
  public abstract Element corresponding_element { get; }
  public abstract UseElement corresponding_use_element { get; }
  public abstract ElementInstance parent_node { get; }
  public abstract ElementInstanceList child_nodes { get; }
  public abstract ElementInstance first_child { get; }
  public abstract ElementInstance last_child { get; }
  public abstract ElementInstance previous_sibling { get; }
  public abstract ElementInstance next_sibling { get; }
}

public interface ElementInstanceList : Object {

  public abstract uint length { get; }

  public abstract ElementInstance item (uint index);
}

public interface ImageElement : Object,
                            Element,
                            URIReference,
                            Tests,
                            LangSpace,
                            ExternalResourcesRequired,
                            Stylable,
                            Transformable {
  public abstract AnimatedLength x { get; set; }
  public abstract AnimatedLength y { get; set; }
  public abstract AnimatedLength width { get; set; }
  public abstract AnimatedLength height { get; set; }
  public abstract AnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
}

public interface SwitchElement : Object,
                             Element,
                             Tests,
                             LangSpace,
                             ExternalResourcesRequired,
                             Stylable,
                             Transformable {
}

public interface GetSVGDocument {
  public abstract Document get_svg_document();
}

} // GSvg
