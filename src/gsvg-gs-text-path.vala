/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using GXml;
using Gee;

public class GSvg.GsTextPathElement : GsTextContentElement,
                                   URIReference,
                                   TextPathElement {
  public AnimatedLength start_off_set { get; set; }
  [Description (nick="::startOffset")]
  public GsAnimatedLength mstart_off_set {
    get { return start_off_set as GsAnimatedLength; }
    set { start_off_set = value as AnimatedLength; }
  }
  public AnimatedEnumeration method { get; set; }
  [Description (nick="::method")]
  public GsAnimatedEnumeration mmethod {
    get { return method as GsAnimatedEnumeration; }
    set { method = value as AnimatedEnumeration; }
  }
  public AnimatedEnumeration spacing { get; set; }
  [Description (nick="::spacing")]
  public GsAnimatedEnumeration mspacing {
    get { return spacing as GsAnimatedEnumeration; }
    set { spacing = value as AnimatedEnumeration; }
  }
  // URIReference
  public AnimatedString href { get; set; }
  [Description (nick="::xlink:href")]
  public GsAnimatedString mhref {
    get { return href as GsAnimatedString; }
    set { href = value as AnimatedString; }
  }
}

