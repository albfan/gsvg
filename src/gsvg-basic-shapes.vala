/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;

public class GSvg.GsRectElementMap : GomHashMap, RectElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct {
    try {
      initialize (typeof (GsRectElement));
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public new RectElement RectElementMap.get (string id) {
    return (this as GomHashMap).get (id) as RectElement;
  }
  public new void append (RectElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to rects collection: %s", e.message);
    }
  }
}

public class GSvg.GsCircleElement : GSvg.GsTransformable,
                          GSvg.CircleElement, MappeableElement
{
  public AnimatedLengthCX cx { get; set; }
  [Description (nick="::cx")]
  public GsAnimatedLengthCX mcx {
    get { return cx as GsAnimatedLengthCX; }
    set { cx = value as AnimatedLengthCX; }
  }
  public AnimatedLengthCY cy { get; set; }
  [Description (nick="::cy")]
  public GsAnimatedLengthCY mcy {
    get { return cy as GsAnimatedLengthCY; }
    set { cy = value as AnimatedLengthCY; }
  }
  public AnimatedLengthR r { get; set; }
  [Description (nick="::r")]
  public GsAnimatedLengthR mr {
    get { return r as GsAnimatedLengthR; }
    set { r = value as AnimatedLengthR; }
  }
  construct {
    initialize ("circle");
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

public class GSvg.GsCircleElementMap : GomHashMap, CircleElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct {
    try {
      initialize (typeof (GsCircleElement));
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public new CircleElement CircleElementMap.get (string id) {
    return (this as GomHashMap).get (id) as CircleElement;
  }
  public new void append (CircleElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to circles collection: %s", e.message);
    }
  }
}

public class GSvg.GsEllipseElement : GSvg.GsTransformable,
                          GSvg.EllipseElement, MappeableElement
{
  public AnimatedLengthCX cx  { get; set; }
  [Description (nick="::cx")]
  public GsAnimatedLengthCX mcx {
    get { return cx as GsAnimatedLengthCX; }
    set { cx = value as AnimatedLengthCX; }
  }
  public AnimatedLengthCY cy { get; set; }
  [Description (nick="::cy")]
  public GsAnimatedLengthCY mcy {
    get { return cy as GsAnimatedLengthCY; }
    set { cy = value as AnimatedLengthCY; }
  }
  public AnimatedLengthRX rx { get; set; }
  [Description (nick="::rx")]
  public GsAnimatedLengthRX mrx {
    get { return rx as GsAnimatedLengthRX; }
    set { rx = value as AnimatedLengthRX; }
  }
  public AnimatedLengthRY ry { get; set; }
  [Description (nick="::ry")]
  public GsAnimatedLengthRY mry {
    get { return ry as GsAnimatedLengthRY; }
    set { ry = value as AnimatedLengthRY; }
  }
  construct {
    try { initialize ("ellipse"); }
    catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

public class GSvg.GsEllipseElementMap : GomHashMap, EllipseElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct { try { initialize (typeof (GsEllipseElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new EllipseElement EllipseElementMap.get (string id) {
    return (this as GomHashMap).get (id) as EllipseElement;
  }
  public new void append (EllipseElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to ellipses collection: %s", e.message);
    }
  }
}

public class GSvg.GsLineElement : GSvg.GsTransformable,
                           GSvg.LineElement, MappeableElement
{
  public AnimatedLengthX x1 { get; set; }
  [Description (nick="::x1")]
  public GsAnimatedLengthX mx1 {
    get { return x1 as GsAnimatedLengthX; }
    set { x1 = value as AnimatedLengthX; }
  }
  public AnimatedLengthY y1 { get; set; }
  [Description (nick="::y1")]
  public GsAnimatedLengthY my1 {
    get { return y1 as GsAnimatedLengthY; }
    set { y1 = value as AnimatedLengthY; }
  }
  public AnimatedLengthX x2 { get; set; }
  [Description (nick="::x2")]
  public GsAnimatedLengthX mx2 {
    get { return x2 as GsAnimatedLengthX; }
    set { x2 = value as AnimatedLengthX; }
  }
  public AnimatedLengthY y2 { get; set; }
  [Description (nick="::y2")]
  public GsAnimatedLengthY my2 {
    get { return y2 as GsAnimatedLengthY; }
    set { y2 = value as AnimatedLengthY; }
  }
  construct {
    initialize ("line");
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

public class GSvg.GsLineElementMap : GomHashMap, LineElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct { try { initialize (typeof (GsLineElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new LineElement LineElementMap.get (string id) {
    return (this as GomHashMap).get (id) as LineElement;
  }
  public new void append (LineElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to lines collection: %s", e.message);
    }
  }
}

public class GSvg.GsPolylineElement : GSvg.GsAnimatedPoints, PolylineElement {
  construct {
    initialize ("polyline");
  }
}

public class GSvg.GsPolylineElementMap : GomHashMap, PolylineElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct { try { initialize (typeof (GsPolylineElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new PolylineElement PolylineElementMap.get (string id) {
    return (this as GomHashMap).get (id) as PolylineElement;
  }
  public new void append (PolylineElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to polylines collection: %s", e.message);
    }
  }
}

public class GSvg.GsPolygonElement : GSvg.GsAnimatedPoints, PolygonElement {
  construct {
    initialize ("polygon");
  }
}


public class GSvg.GsPolygonElementMap : GomHashMap, PolygonElementMap {
  public int length { get { return (this as GomHashMap).length; } }
  construct { try { initialize (typeof (GsPolygonElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new PolygonElement PolygonElementMap.get (string id) {
    return (this as GomHashMap).get (id) as PolygonElement;
  }
  public new void append (PolygonElement el) {
    try {
      (this as GomHashMap).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to polygons collection: %s", e.message);
    }
  }
}

