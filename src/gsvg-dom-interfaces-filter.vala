/* gsvg-dom-public interfaces-filter.vala
 *
 * Copyright (C) 2016,2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;

namespace GSvg {

public interface FilterElement : Object, Element,
                             URIReference,
                             LangSpace,
                             ExternalResourcesRequired,
                             Stylable {

  public abstract AnimatedEnumeration filter_units { get; }
  public abstract AnimatedEnumeration primitive_units { get; }
  public abstract AnimatedLength x { get; }
  public abstract AnimatedLength y { get; }
  public abstract AnimatedLength width { get; }
  public abstract AnimatedLength height { get; }
  public abstract AnimatedInteger filter_res_x { get; }
  public abstract AnimatedInteger filter_res_y { get; }

  public abstract void setFilterRes(uint filterResX, uint filterResY) throws GLib.Error;
}

public interface FilterPrimitiveStandardAttributes : Object, Stylable {
  public abstract AnimatedLength x { get; }
  public abstract AnimatedLength y { get; }
  public abstract AnimatedLength width { get; }
  public abstract AnimatedLength height { get; }
  public abstract AnimatedString result { get; }
}

public interface FEBlendElement : Object, Element,
                              FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedString in2 { get; }
  public abstract AnimatedEnumeration mode { get; }
}

  // Blend Mode Types
public enum FEBlendMode {
  UNKNOWN = 0,
  NORMAL = 1,
  MULTIPLY = 2,
  SCREEN = 3,
  DARKEN = 4,
  LIGHTEN = 5
}

public interface FEColorMatrixElement : Object, Element,
                                    FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedEnumeration cm_type { get; }
  public abstract AnimatedNumberList values { get; }
}

  // Color Matrix Types
public enum FEColorMatrixType {
  UNKNOWN = 0,
  MATRIX = 1,
  SATURATE = 2,
  HUEROTATE = 3,
  LUMINANCETOALPHA = 4
}


public interface FEComponentTransferElement : Object, Element,
                                          FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
}

public interface ComponentTransferFunctionElement : Object, Element {
  public abstract AnimatedEnumeration ctf_type { get; }
  public abstract AnimatedNumberList table_values { get; }
  public abstract AnimatedNumber slope { get; }
  public abstract AnimatedNumber intercept { get; }
  public abstract AnimatedNumber amplitude { get; }
  public abstract AnimatedNumber exponent { get; }
  public abstract AnimatedNumber offset { get; }
}


  // Component Transfer Types
public enum FEComponentTransferType {
  UNKNOWN = 0,
  IDENTITY = 1,
  TABLE = 2,
  DISCRETE = 3,
  LINEAR = 4,
  GAMMA = 5
}


public interface FEFuncRElement : Object, ComponentTransferFunctionElement {
}

public interface FEFuncGElement : Object, ComponentTransferFunctionElement {
}

public interface FEFuncBElement : Object, ComponentTransferFunctionElement {
}

public interface FEFuncAElement : Object, ComponentTransferFunctionElement {
}

public interface FECompositeElement : Object,
                                  Element,
                                  FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedString in2 { get; }
  public abstract AnimatedEnumeration operator { get; }
  public abstract AnimatedNumber k1 { get; }
  public abstract AnimatedNumber k2 { get; }
  public abstract AnimatedNumber k3 { get; }
  public abstract AnimatedNumber k4 { get; }
}


  // Composite Operators
public enum FE0CompositeOperator {
  UNKNOWN = 0,
  OVER = 1,
  IN = 2,
  OUT = 3,
  ATOP = 4,
  XOR = 5,
  ARITHMETIC = 6,
}

public interface FEConvolveMatrixElement : Object,
                                      Element,
                                      FilterPrimitiveStandardAttributes {

  public abstract AnimatedString in1 { get; }
  public abstract AnimatedInteger order_x { get; }
  public abstract AnimatedInteger order_y { get; }
  public abstract AnimatedNumberList kernel_matrix { get; }
  public abstract AnimatedNumber divisor { get; }
  public abstract AnimatedNumber bias { get; }
  public abstract AnimatedInteger target_x { get; }
  public abstract AnimatedInteger target_y { get; }
  public abstract AnimatedEnumeration edge_mode { get; }
  public abstract AnimatedNumber kernel_unit_length_x { get; }
  public abstract AnimatedNumber kernel_unit_length_y { get; }
  public abstract AnimatedBoolean preserve_alpha { get; }
}

  // Edge Mode Values
public enum EdgeMode {
  UNKNOWN = 0,
  DUPLICATE = 1,
  WRAP = 2,
  NONE = 3
}

public interface FEDiffuseLightingElement : Object,
                                        Element,
                                        FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedNumber surface_scale { get; }
  public abstract AnimatedNumber diffuse_constant { get; }
  public abstract AnimatedNumber kernel_unit_length_x { get; }
  public abstract AnimatedNumber kernel_unit_length_y { get; }
}

public interface FEDistantLightElement : Object, Element {
  public abstract AnimatedNumber azimuth { get; }
  public abstract AnimatedNumber elevation { get; }
}

public interface FEPointLightElement : Object, Element {
  public abstract AnimatedNumber x { get; }
  public abstract AnimatedNumber y { get; }
  public abstract AnimatedNumber z { get; }
}

public interface FESpotLightElement : Object, Element {
  public abstract AnimatedNumber x { get; }
  public abstract AnimatedNumber y { get; }
  public abstract AnimatedNumber z { get; }
  public abstract AnimatedNumber points_at_x { get; }
  public abstract AnimatedNumber points_at_y { get; }
  public abstract AnimatedNumber points_at_z { get; }
  public abstract AnimatedNumber specular_exponent { get; }
  public abstract AnimatedNumber limiting_cone_angle { get; }
}

public interface FEDisplacementMapElement : Object, Element,
                                        FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedString in2 { get; }
  public abstract AnimatedNumber scale { get; }
  public abstract AnimatedEnumeration x_channel_selector { get; }
  public abstract AnimatedEnumeration y_channel_selector { get; }
}

  // Channel Selectors
public enum Channel {
  UNKNOWN = 0,
  R = 1,
  G = 2,
  B = 3,
  A = 4
}


public interface FEFloodElement : Object, Element,
                              FilterPrimitiveStandardAttributes {
}

public interface FEGaussianBlurElement : Object, Element,
                                     FilterPrimitiveStandardAttributes {

  public abstract AnimatedString in1 { get; }
  public abstract AnimatedNumber std_deviation_x { get; }
  public abstract AnimatedNumber std_deviation_y { get; }

  public abstract void set_std_deviation (double stdDeviationX, double stdDeviationY) throws GLib.Error;
}

public interface FEImageElement : Object, Element,
                              URIReference,
                              LangSpace,
                              ExternalResourcesRequired,
                              FilterPrimitiveStandardAttributes {
  public abstract AnimatedPreserveAspectRatio preserve_aspect_ratio { get; }
}

public interface FEMergeElement : Object, Element,
                              FilterPrimitiveStandardAttributes {
}

public interface FEMergeNodeElement : Object, Element {
  public abstract AnimatedString in1 { get; }
}

public interface FEMorphologyElement : Object, Element,
                                   FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedEnumeration operator { get; }
  public abstract AnimatedNumber radius_x { get; }
  public abstract AnimatedNumber radius_y { get; }
}


  // Morphology Operators
public enum MorphologyOperator {
  UNKNOWN = 0,
  ERODE = 1,
  DILATE = 2
}

public interface FEOffsetElement : Object, Element,
                               FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedNumber dx { get; }
  public abstract AnimatedNumber dy { get; }
}

public interface FESpecularLightingElement : Object, Element,
                                         FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
  public abstract AnimatedNumber surface_scale { get; }
  public abstract AnimatedNumber specular_constant { get; }
  public abstract AnimatedNumber specular_exponent { get; }
  public abstract AnimatedNumber kernel_unit_length_x { get; }
  public abstract AnimatedNumber kernel_unit_length_y { get; }
}

public interface FETileElement : Object, Element,
                             FilterPrimitiveStandardAttributes {
  public abstract AnimatedString in1 { get; }
}

public interface FETurbulenceElement : Object, Element,
                                   FilterPrimitiveStandardAttributes {
  public abstract AnimatedNumber base_frequency_x { get; }
  public abstract AnimatedNumber base_frequency_y { get; }
  public abstract AnimatedInteger num_octaves { get; }
  public abstract AnimatedNumber seed { get; }
  public abstract AnimatedEnumeration stitch_tiles { get; }
  public abstract AnimatedEnumeration ttype { get; }
}


  // Turbulence Types
public enum TurbulenceType {
  UNKNOWN = 0,
  FRACTALNOISE = 1,
  TURBULENCE = 2
}
  // Stitch Options
public enum StichType {
  UNKNOWN = 0,
  STITCH = 1,
  NOSTITCH = 2
}

} // GSvg
