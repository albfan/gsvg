/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-path-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/path/write",
    ()=>{
      try {
        var svg = new GSvg.GsSVGElement ();
        var p = svg.create_path ("M 100 100 L 200 200 L 300 100 z", null);
        assert (p is PathElement);
        message ((p as GomElement).write_string ());
        svg.append_child (p);
        message (svg.write_string ());
        assert ("""<svg xmlns="http://www.w3.org/2000/svg"><path d="M 100 100 L 200 200 L 300 100 z"/>""" in svg.write_string ());
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/path/read",
    ()=>{
      try {
        var svg = new GSvg.GsSVGElement ();
        svg.read_from_string ("""<svg xmlns="http://www.w3.org/2000/svg"><path id="path" d="M 100 100 L 200 200 L 300 100 z"/></svg>""");
        assert (svg.child_nodes.length == 1);
        var p = svg.child_nodes.item (0) as PathElement;
        assert (p != null);
        assert (p is PathElement);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
